
// Вызов нечто попапообразного (с проставлением в <body> класса убирающего прокрутку)
// Cтили для попапообразного в css

function toggleModal(target) {
	if (target.parent("body").length < 1) {
		target.appendTo("body");
	}
	// таймаут чтоб анимация подцепилась
	setTimeout(function() {
		target.toggleClass("show");
		$("body").toggleClass("no-scroll");
		if(!target.hasClass("show")) {
			setTimeout(function() { target.css("visibility", "hidden"); }, 500);
		} else {
			target.css("visibility", "visible");
		}
	}, 10);
}

$(document).on("click", "*[data-toggle-modal]", function() {
	toggleModal($($(this).attr("data-toggle-modal")));
});

$(document).on("click", ".modal", function(event) {
	console.log("zok", event.target);
	if($(event.target).hasClass("modal")) {
		toggleModal($(event.target));
	}
});

$(document).on("click", "*[data-close-modal]", function() {
	toggleModal($(this).parents(".modal").eq(0));
});
