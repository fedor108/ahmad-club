angular.module("AhmadClubApp", ["ui.router"]);


/*	Init */

angular.module("AhmadClubApp")
	.constant("TmplPath", "/content/")
	.value("Tests", [
		{
			id: "CityOfYourDream",
			title: "Город вашей мечты",
			questions: [
				{
					title: "Какой стиль в одежде Вы предпочитаете?",
					answers: ["Деловой", "Casual", "Романтический", "Классический"]
				},
				{
					title: "Какая погода нравится Вам больше всего?",
					answers: ["Солнечная", "Пасмурная", "Ясная", "Тихая"]
				},
				{
					title: "Ваше обычное настроение?",
					answers: ["Веселое", "Спокойное", "Счастливое", "Мечтательное"]
				}
			]

		},
		{
			id: "RestOfYourDream",
			title: "О каком отдыхе вы мечтаете?",
			questions: [
				{
					title: "Что занимает большую часть времени в Вашей жизни?",
					answers: ["Мечты", "Общение", "Работа", "Спорт"]
				},
				{
					title: "Ваши друзья думают, что Вы:",
					answers: ["Романтичны", "Дружелюбны", "Любознательны", "Любите приключения"]
				},
				{
					title: "Какую дорогу Вы бы предпочли?",
					answers: ["Скоростную трассу", "Не имеет значение, главное, чтобы была короткой", "Проселочную дорогу по красивой местности", "Морское побережье"]
				}
			]
		},
		{
			id: "OptimistQuiz",
			title: "Кто Вы - оптимист, пессимист или реалист?",
			questions: [
				{
					title: "Как Вы относитесь к фразе «Если неприятность может произойти, она обязательно случится»?",
					answers: ["Считаете, что фраза хорошо иллюстрирует реальность", "Уверены, что это не так", "Полагаете, что все зависит от обстоятельств"]
				},
				{
					title: "Насколько часто у Вас бывает плохое настроение?",
					answers: ["Довольно часто", "Бывает, когда есть причина", "Практически никогда"]
				}
			]
		}
	])
	.run(["$rootScope", "$state", "$stateParams", "TmplPath", function($rootScope, $state, $stateParams, TmplPath) {
	    // It's very handy to add references to $state and $stateParams to the $rootScope
	    // so that you can access them from any scope within your applications.For example,
	    // <li ng-class="{ active: $state.includes('contacts.list') }"> will set the <li>
	    // to active whenever 'contacts.list' or one of its decendents is active.
	    $rootScope.$state = $state;
	    $rootScope.$stateParams = $stateParams;
	}]);


/*	States */

angular.module("AhmadClubApp")
	.config(["$stateProvider", "$urlRouterProvider", "TmplPath", function($stateProvider, $urlRouterProvider, TmplPath) {

		$urlRouterProvider
			.when("","/index")
			.when("/", "/index")
			.when("/contest", "/contest/general")
			.when("/contest/", "/contest/general")
			.when("/teatime", "/teatime/test/CityOfYourDream")
			.when("/teatime/", "/teatime/test/CityOfYourDream")
			.when("/teatime/test", "/teatime/test/CityOfYourDream")
			.when("/teatime/test/", "/teatime/test/CityOfYourDream")
			.when("/cabinet", "/cabinet/score")
			.when("/cabinet/", "/cabinet/score")
			.when("/auth", "/auth/registration")
			.when("/auth/", "/auth/registration")
			.when("/help", "/help/faq")
			.when("/help/", "/help/faq")
			.otherwise("/");

		$stateProvider
			.state("index", {
				url: "/index",
				views: {
					"main": {
						templateUrl: TmplPath + "index.html"
					}
				}
			})
			.state("indexpromo", {
				url: "/indexpromo",
				views: {
					"main": {
						templateUrl: TmplPath + "index-promo.html"
					}
				}
			})
			.state("indexcontestend", {
				url: "/indexcontestend",
				views: {
					"main": {
						templateUrl: TmplPath + "index-contest-end.html"
					}
				}
			})
			.state("server404", {
				url: "/404",
				views: {
					"main": {
						templateUrl: TmplPath + "404.html"
					}
				}
			})
			.state("contest", {
				abstract: true,
				url: "/contest",
				views: {
					"main": {
						templateUrl: TmplPath + "contest.html"
					}
				}
			})
			.state("contest.general", {
				url: "/general",
				views: {
					"content": {
						templateUrl: TmplPath + "contest.general.html"
					}
				}
			})
			.state("contest.works", {
				url: "/works",
				views: {
					"content": {
						templateUrl: TmplPath + "contest.works.html"
					}
				}
			})
			.state("contest.work", {
				url: "/work/:work",
				views: {
					"content": {
						templateUrl: TmplPath + "contest.work.html",
						controller: ["$stateParams", "$scope", "Tests", function($stateParams, $scope, Tests) {
							console.log($stateParams);
						}]
					}
				}
			})
			.state("contest.rating", {
				url: "/rating",
				views: {
					"content": {
						templateUrl: TmplPath + "contest.rating.html"
					}
				}
			})
			.state("contest.winners", {
				url: "/winners",
				views: {
					"content": {
						templateUrl: TmplPath + "contest.winners.html"
					}
				}
			})
			.state("contest.jury", {
				url: "/jury",
				views: {
					"content": {
						templateUrl: TmplPath + "contest.jury.html"
					}
				}
			})
			.state("teatime", {
				abstract: true,
				url: "/teatime",
				views: {
					"main": {
						templateUrl: TmplPath + "teatime.html"
					}
				}
			})
			.state("teatime.test", {
				url: "/test/:test?result",
				views: {
					"content": {
						templateUrl: TmplPath + "teatime.test.html",
						controller: ["$stateParams", "$scope", "Tests", function($stateParams, $scope, Tests) {
							$scope.currentTest = getItemByKey(Tests, "id", $stateParams.test);
							$scope.currentQuestion = $scope.currentTest.questions[0];
							$scope.allTests = Tests;
							function getItemByKey(arr, key, value) {
								for(var i = 0; i < arr.length; i++) {
									if(arr[i][key] == value) {
										return arr[i];
									}
								}
								return null;
							}
						}]
					}
				}
			})
			.state("teatime.wallpapers", {
				url: "/wallpapers",
				views: {
					"content": {
						templateUrl: TmplPath + "teatime.wallpapers.html"
					}
				}
			})
			.state("teatime.magic", {
				url: "/magic",
				views: {
					"content": {
						templateUrl: TmplPath + "teatime.magic.html"
					}
				}
			})
			.state("teatime.valentine", {
				url: "/valentine",
				views: {
					"content": {
						templateUrl: TmplPath + "teatime.valentine.html"
					}
				}
			})


			.state("catalog", {
				url: "/catalog",
				views: {
					"main": {
						templateUrl: TmplPath + "catalog.html"
					}
				}
			})
			.state("teacard", {
				url: "/teacard",
				views: {
					"main": {
						templateUrl: TmplPath + "teacard.html"
					}
				}
			})
			.state("help", {
				abstract: true,
				url: "/help",
				views: {
					"main": {
						templateUrl: TmplPath + "help.html"
					}
				}
			})
			.state("help.rules", {
				url: "/faq",
				views: {
					"content": {
						templateUrl: TmplPath + "help.faq.html"
					}
				}
			})
			.state("help.feedback", {
				url: "/feedback",
				views: {
					"content": {
						templateUrl: TmplPath + "help.feedback.html"
					}
				}
			})
			.state("help.rules-loyalty", {
				url: "/rules-loyalty",
				views: {
					"content": {
						templateUrl: TmplPath + "help.rules-loyalty.html"
					}
				}
			})
			.state("help.rules-contest", {
				url: "/rules-contest",
				views: {
					"content": {
						templateUrl: TmplPath + "help.rules-contest.html"
					}
				}
			})

			.state("cabinet", {
				abstract: true,
				url: "/cabinet",
				views: {
					"main": {
						templateUrl: TmplPath + "cabinet.html"
					}
				}
			})
			.state("cabinet.score", {
				url: "/score",
				views: {
					"content": {
						templateUrl: TmplPath + "cabinet.score.html"
					}
				}
			})
			.state("cabinet.gifts", {
				url: "/gifts",
				views: {
					"content": {
						templateUrl: TmplPath + "cabinet.gifts.html"
					}
				}
			})
			.state("cabinet.works", {
				url: "/works",
				views: {
					"content": {
						templateUrl: TmplPath + "cabinet.works.html"
					}
				}
			})
			.state("cabinet.share", {
				url: "/share",
				views: {
					"content": {
						templateUrl: TmplPath + "cabinet.share.html"
					}
				}
			})
			.state("cabinet.sharenoauthorization", {
				url: "/sharenoauthorization",
				views: {
					"content": {
						templateUrl: TmplPath + "cabinet.sharenoauthorization.html"
					}
				}
			})
			.state("cabinet.newwork", {
				url: "/newwork",
				views: {
					"content": {
						templateUrl: TmplPath + "cabinet.newwork.html",
					}
				}
			})
			.state("cabinet.edit", {
				url: "/edit",
				views: {
					"content": {
						templateUrl: TmplPath + "cabinet.edit.html"
					}
				}
			})

			.state("auth", {
				abstract: true,
				url: "/auth",
				views: {
					"main": {
						templateUrl: TmplPath + "auth.html"
					}
				}
			})
			.state("auth.registration", {
				url: "/registration",
				views: {
					"content": {
						templateUrl: TmplPath + "auth.registration.html"
					}
				}
			})
			.state("auth.login", {
				url: "/login",
				views: {
					"content": {
						templateUrl: TmplPath + "auth.login.html"
					}
				}
			})
			.state("auth.restore-by-phone", {
				url: "/restore-by-phone",
				views: {
					"content": {
						templateUrl: TmplPath + "auth.restore-by-phone.html"
					}
				}
			})
			.state("auth.restore-by-email", {
				url: "/restore-by-email",
				views: {
					"content": {
						templateUrl: TmplPath + "auth.restore-by-email.html"
					}
				}
			})

	}]);