// Превью загрузки фотки

function PreviewImage(inputElement, previewElement) {
	if(!window.FileReader) return;

	var oFReader = new FileReader();
	oFReader.readAsDataURL(inputElement.files[0]);

	oFReader.onload = function (oFREvent) {
		previewElement.style.backgroundImage =  "url(" + oFREvent.target.result + ")";
		if(previewElement.classList) {
			previewElement.classList.add("loaded");
		}
	};
}